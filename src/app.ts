import express from "express"
import bootstrap from "./startup/bootstrap"
const app = express()

bootstrap(app)
